cmake_minimum_required(VERSION 3.16)
project(mm_boost)

set(CMAKE_CXX_STANDARD 17)

add_executable(mmbunco mmboost.cpp)
add_executable(mmbcompr mmboost.cpp)

set_target_properties( mmbunco PROPERTIES COMPILE_FLAGS "-DMM_BOOST_COMPRESSION=0" )
set_target_properties( mmbcompr PROPERTIES COMPILE_FLAGS "-DMM_BOOST_COMPRESSION=1" )
