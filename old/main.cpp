#include <fstream>
#include <iostream>
#include <vector>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_sparse.hpp>

namespace ublas = boost::numeric::ublas;

template <class val_t>
boost::numeric::ublas::matrix<val_t> read_mat(
        std::string &filename
        ,const size_t rows
        ,const size_t cols
){
    std::string line;
    std::ifstream infile;
    infile.open (filename.c_str());
    assert(infile.is_open());

    boost::numeric::ublas::matrix<val_t> out(rows,cols);
    for(size_t r=0; r<rows; ++r) {
        for(size_t c=0; c<cols; ++c) {
            char *ch_ptr = (char *)(&out(r,c));
            for(size_t p=0;p<sizeof(val_t); ++p){
                infile.get(*ch_ptr);
                ++ ch_ptr;
            }
        }
    }
    infile.close();
    return out;
}

//https://stackoverflow.com/questions/3765026/boost-compressed-matrix-basics
template <class val_t>
boost::numeric::ublas::compressed_matrix<val_t> read_sparse_mat(
        std::string &filename
        ,const size_t rows
        ,const size_t cols
){
    std::string line;
    std::ifstream infile;

    //count nonzeros
    size_t nnz = 0;
    infile.open (filename.c_str());
    assert(infile.is_open());
    for(size_t r=0; r<rows; ++r) {
        for(size_t c=0; c<cols; ++c) {
            val_t v;
            char *ch_ptr = (char *)(&v);
            for(size_t p=0;p<sizeof(val_t); ++p){
                infile.get(*ch_ptr);
                ++ ch_ptr;
            }
            nnz += (v!=0);
        }
    }
    infile.close();

    //load matrix
    infile.open (filename.c_str());
    assert(infile.is_open());
    boost::numeric::ublas::compressed_matrix<val_t> out(rows,cols,nnz);
    for(size_t r=0; r<rows; ++r) {
        for(size_t c=0; c<cols; ++c) {
            val_t v;
            char *ch_ptr = (char *)(&v);
            for(size_t p=0;p<sizeof(val_t); ++p){
                infile.get(*ch_ptr);
                ++ ch_ptr;
            }
            if (v) {
                out(r,c) = v;
            }
        }
    }
    infile.close();

    return out;
}

template <class matrix_t>
void print_mat(matrix_t &matrix) {
    for (size_t i=0; i < matrix.size1(); i++) {
        for (size_t j=0; j < matrix.size2(); j++) {
            std::cout << matrix(i, j);
            if(j+1 != matrix.size2()) {
                std::cout << "\t";
            }
        }
        std::cout << std::endl;
    }
    return;
}

template <class vec_t>
void print_vec(vec_t &vec) {
    for (size_t i=0; i < vec.size(); i++) {
        std::cout << vec(i) << "\t";
    }
    std::cout << std::endl;
    return;
}

int main (int argc, char* argv[]) {
    if (argc != 1+3){
        std::cout << "Usage is: " << argv[0] << " <infilepath> <rows> <cols>" << std::endl;
        exit(-1);
    }
    //args
    std::string infilepath = argv[1];
    const size_t rows   = std::stoul(argv[2]);
    const size_t cols   = std::stoul(argv[3]);
    //const size_t blocks = std::stoul(argv[4]);

    //init
    using value_t = double;
    value_t lambda = 1;
    ublas::vector<value_t> U(rows), V(cols); //init to zero
    for(size_t i=0; i<V.size(); ++i ){V(i) = 1;}
    /*std::vector<ublas::vector<value_t>> VS;
    VS.reserve(blocks);
    for(size_t i=0; i<blocks; ++i ){VS.emplace_back(cols);}*/

    //params
    const size_t ITERS = 500;
    /*const size_t row_block_size = (rows + blocks - 1) / blocks;
    const size_t row_bgn = tid * row_block_size;
    const size_t row_end = std::min<size_t>((tid + 1) * row_block_size, rows);
    assert(row_end > row_bgn);*/

#if MM_BOOST_COMPRESSION
    auto A = read_sparse_mat<value_t>(infilepath, rows, cols );
#else
    auto A = read_mat<value_t>(infilepath, rows, cols );
#endif

    //boost::numeric::ublas::axpy_prod(A, V, C);

    for(size_t iter=0; iter<ITERS; ++iter) {
        V /= lambda;
        U = ublas::prod(A, V);
        V = ublas::prod(U, A);
        auto it = std::max_element(V.begin(), V.end() );
        lambda = *it;
    }
    //V += ublas::prod(A, U);
    /*
    std::cout << "A:" << std::endl;
    print_mat(A);
    std::cout << "U:" << std::endl;
    print_vec(U);
    std::cout << "V:" << std::endl;
    print_vec(V);
     */

    std::cout << "λ: " << lambda << std::endl;
#if MM_BOOST_COMPRESSION
    std::cout << "(compressed)" << std::endl;
#else
    std::cout << "(uncompressed)" << std::endl;
#endif

	return 0;
}