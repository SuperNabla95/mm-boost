import struct

def write_binary_sequence(filename, data):
    with open(filename, 'wb') as file:
        for value in data:
            binary_value = struct.pack('d', value)
            file.write(binary_value)

data = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0]
filename = 'sequence.bin'

write_binary_sequence(filename, data)