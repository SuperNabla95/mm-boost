#include <iostream>
#include <fstream>
//#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_sparse.hpp>
//#include <boost/numeric/ublas/vector.hpp>
//#include <boost/numeric/ublas/io.hpp>
//#include <boost/numeric/ublas/vector_expression.hpp>

namespace ublas = boost::numeric::ublas;

template <class matrix_t>
matrix_t read_binmat(
    const std::string& filename, 
    std::size_t m,
    std::size_t n
    ) {
    using value_t = typename matrix_t::value_type;

    std::ifstream file(filename, std::ios::binary);
    if (!file.is_open()) {
        throw std::runtime_error("Impossibile aprire il file binario.");
    }

    matrix_t matrix(m, n);

    for (std::size_t i = 0; i < m; ++i) {
        for (std::size_t j = 0; j < n; ++j) {
            double v;
            file.read(reinterpret_cast<char*>(&v), sizeof(value_t));
            matrix(i, j) = v;
        }
    }

    file.close();
    return matrix;
}

template <class vector_t>
vector_t read_binvec(
    const std::string& filename, 
    std::size_t m
    ) {
    using value_t = typename vector_t::value_type;

    std::ifstream file(filename, std::ios::binary);
    if (!file.is_open()) {
        throw std::runtime_error("Impossibile aprire il file binario.");
    }

    vector_t vec(m);

    for (std::size_t i = 0; i < m; ++i) {
        double v;
        file.read(reinterpret_cast<char*>(&v), sizeof(value_t));
        vec(i) = v;
    }

    file.close();
    return vec;
}

int main(int argc, char *argv[]) {
    if (argc != 1+3) {
        std::cout << "Usage is: " << argv[0] << " <infilepath> <rows> <cols>";
        exit(1);
    }
    //args
    std::string infilepath = argv[1];
    const size_t rs = std::stoul(argv[2]), cs = std::stoul(argv[3]);

    //params
    const size_t niters = 500;

    using value_t = double;
    std::cout << "infilepath: " << infilepath << "\nniters: " << niters << std::endl; 
#if MM_BOOST_COMPRESSION
    std::cout << "boost compressed version." << std::endl;
    using matrix_t = ublas::compressed_matrix<value_t>;
#else
    std::cout << "boost uncomppressed version." << std::endl;
    using matrix_t = ublas::matrix<value_t>;
#endif

    using vector_t = ublas::vector<value_t>;

    matrix_t mat = read_binmat<matrix_t>(infilepath,rs,cs);
    vector_t xs = read_binvec<vector_t>(infilepath,cs);

#if DEBUG
    //debug mat
    for(size_t r=0; r<rs; ++r) {
        for(size_t c=0; c<cs; ++c) {
            std::cout << mat(r,c) << " ";
        }
        std::cout << std::endl;
    }
    //debug xs
    for(size_t c=0; c<cs; ++c) {
        std::cout << xs(c) << " ";
    }
    std::cout << std::endl;
#endif

    // Definisci il vettore 
    vector_t ys(rs, 0.0);
    value_t lambda = 1;

    // Esegui la moltiplicazione tra matrice e vettore
    for(size_t iter=0; iter<niters; ++iter) {
        xs /= lambda;
        ys = ublas::prod(mat, xs);
        xs = ublas::prod(ys, mat);
        lambda = *std::max_element(xs.begin(), xs.end());
    }

    // Stampa il risultato
    std::cout << "lambda: " << lambda << std::endl;

    return 0;
}